set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

set nu
syntax on
set hidden

" Indentation preferences

filetype plugin indent on
set tabstop=4
set shiftwidth=4
set expandtab

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

Plugin 'bling/vim-airline'
Plugin 'mattn/emmet-vim'
Plugin 'majutsushi/tagbar'
Plugin 'kien/ctrlp.vim'
Plugin 'moll/vim-node'
Plugin 'jelera/vim-javascript-syntax'
" Plugin 'vim-scripts/JavaScript-Indent'
Plugin 'pangloss/vim-javascript'
Plugin 'digitaltoad/vim-jade'
Plugin 'marijnh/tern_for_vim'
Plugin 'hail2u/vim-css3-syntax'
Plugin 'cakebaker/scss-syntax.vim'

let g:EclimCompletionMethod = 'omnifunc'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" YouCompleteMe
    let g:ycm_autoclose_preview_window_after_completion = 1

" CTRLP
	"let g:ctrlp_cmd = 'CtrlPMixed'
	let g:ctrlp_buffer_func = { 'enter': 'MyCtrlPMappings' }
	
	nnoremap <Tab> :bnext<CR>
	nnoremap <S-Tab> :bprevious<CR>
	nnoremap <S-w> :bd<CR>

	func! MyCtrlPMappings()
		nnoremap <buffer> <silent> <c-@> :call <sid>DeleteBuffer()<cr>
	endfunc

    	func! s:DeleteBuffer()
		let line = getline('.')
		let bufid = line =~ '\[\d\+\*No Name\]$' ? str2nr(matchstr(line, '\d\+'))
			\ : fnamemodify(line[2:], ':p')
		exec "bd" bufid
		exec "norm \<F5>"
	endfunc


" AIRLINE 
    let g:airline_symbols = {}
    let g:airline_left_sep = '⮀'
    let g:airline_left_alt_sep = '⮁'
    let g:airline_right_sep = '⮂'
    let g:airline_right_alt_sep = '⮃'
    let g:airline_symbols.branch = '⭠'
    let g:airline_symbols.readonly = '⭤'
    let g:airline_symbols.linenr = '⭡'	
	
    let g:airline#extensions#tabline#enabled = 1	
	set laststatus=2

	" Fancy powerline symbols, needs a patched/edited font
	let g:Powerline_symbols = 'fancy'
	"
	" " Use ? to indicate branches
	"let g:Powerline_symbols_override = {
	"     \ 'BRANCH': [0x2213],
					 \ }
" LIGHTLINE
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'component': {
      \   'readonly': '%{&readonly?"?":""}',
      \ },
      \ 'separator': { 'left': '?', 'right': '?' },
      \ 'subseparator': { 'left': '?', 'right': '?' }
      \ }

" WORD PROCESSING {
	set formatoptions=1
	set lbr
	set linebreak
	set wrap
	au BufRead,BufNewFile *.todo setfiletype todo


	cabbr wp call Wp()
	fun! Wp()
		set formatoptions=1
		set lbr
		set linebreak
		set wrap
		set nonumber
		nnoremap j gj
		nnoremap k gk
		nnoremap 0 g0
		nnoremap $ g$
		set comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:>,fb:-,fb:[+],fb:[x],fb:[-]
		set comments +=fb:-
		set spell spelllang=en_us

	endfu

	" Search for selected text, forwards or backwards.
	vnoremap <silent> * :<C-U>
	  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
	  \gvy/<C-R><C-R>=substitute(
	  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
	  \gV:call setreg('"', old_reg, old_regtype)<CR>
	vnoremap <silent> # :<C-U>
	  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
	  \gvy?<C-R><C-R>=substitute(
	  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
	  \gV:call setreg('"', old_reg, old_regtype)<CR>
"}
