source ~/.config/fish/nvm-wrapper/nvm.fish

set -l name "not_broken"
if [ "$fish_user_environment" != "$name" ]
  set -U fish_color_host -o cyan
  set -U fish_color_status red
  set -U fish_color_user -o green

  set -U fish_user_environment $name

  set -U -x SXHKD_SHELL /bin/sh
end
